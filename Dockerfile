FROM node:16
LABEL maintainer="ruben.bimberg@mni.thm.de"
LABEL version="0.1"
LABEL description="Frontend for visualizing spacy dependencies."

COPY ./server/assets/css /app/server/assets/css
COPY build_sassc.sh /app/build_sassc.sh
RUN bash /app/build_sassc.sh
COPY . /app
RUN cd /app && make clean && make

EXPOSE 8080
CMD ["/usr/bin/make", "--directory", "/app", "start"]