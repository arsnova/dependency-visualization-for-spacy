//- ----------------------------------
//- 💥 DISPLACY DEMO
//- ----------------------------------

'use strict';

{
    const defaultText = 'displaCy uses JavaScript, SVG and CSS to show you how computers understand language';
    const defaultModel = 'en';
    const loading = () => document.body.classList.toggle('loading');
    const onError = (err) => $('#error').style.display = 'block';

    const displacy = new displaCy('', {
        container: '#displacy',
        engine: 'spacy',
        defaultText: defaultText,
        defaultModel: defaultModel,
        collapsePunct: true,
        collapsePhrase: true,
        distance: 200,
        offsetX: 150,
        arrowSpacing: 10,
        arrowWidth: 8,
        arrowStroke: 2,
        wordSpacing: 40,
        font: 'inherit',
        color: '#f5f4f0',
        bg: '#272822',
        onStart: loading,
        onSuccess: loading
    });


    // UI

    const $ = document.querySelector.bind(document);

    let modelData = [];

    // First Run

    const getParams = () => {
        const text = getQueryVar('text') || getQueryVar('full') || getQueryVar('manual') || getQueryVar('steps') || defaultText;
        const model = getQueryVar('model') || defaultModel;
        const collapsePunct = (getQueryVar('cpu')) ? (getQueryVar('cpu') == 0 ? 0 : 1) : 1;
        const collapsePhrase = (getQueryVar('cph')) ? (getQueryVar('cph') == 0 ? 0 : 1) : 1;
        const server = getQueryVar('server') || 'localhost';
        const depApi = getQueryVar('depApi') || '/dep';
        const modelApi = getQueryVar('modelApi') || '/models';

        return [text, model, { collapsePhrase, collapsePunct }, server, depApi, modelApi];
    }

    document.addEventListener('DOMContentLoaded', () => {
        const args = getParams();

        if (getQueryVar('text')) updateView(...args);
        if (getQueryVar('full') || getQueryVar('manual') || getQueryVar('steps')) updateURL(...args);
        if (getQueryVar('server') || getQueryVar('depApi') || getQueryVar('modelApi')) fetchModels(args[3], args[4], args[5]);
    });


    // Run Demo

    const run = (
        text = $('#input').value || defaultText,
        model = $('[name="model"]:checked').value || defaultModel,
        settings = {
            collapsePunct: $('#settings-punctuation').checked,
            collapsePhrase: $('#settings-phrases').checked
        },
        server = $('#serverInput').value,
        depApi = $('#depInput').value,
        modelApi = $('#modelInput').value) => {
        displacy.parse(text, model, settings);
        updateView(text, model, settings, server, depApi, modelApi);
        updateURL(text, model, settings, server, depApi, modelApi);
    }

    const fetchModels = (server, depApi, modelApi) => {
        const onFail = (err) => {
            console.error('Error occured:', err);
            onError(err);
        };
        let xhr = new XMLHttpRequest();
        xhr.open('GET', server + modelApi, true);
        xhr.setRequestHeader('Content-type', 'text/plain');
        xhr.onreadystatechange = () => {
            if(xhr.readyState === 4 && xhr.status === 200) {
                modelData = JSON.parse(xhr.responseText);
                displacy.updateAPI(server + depApi, modelData);
                $('#inputForm').style.display = '';
                $('#error').style.display = '';
                const args = getParams();
                args[3] = server;
                args[4] = depApi;
                args[5] = modelApi;
                updateView(...args);
                updateURL(...args);
            } else if (xhr.status !== 200) {
                onFail(xhr.statusText);
            }
        };
        xhr.onerror = () => {
            xhr.abort();
            onFail('Unknown');
        };
        xhr.send();
    };

    // UI Event Listeners
    $('#inputRowSubmit').addEventListener('click', ev => fetchModels(
        $('#serverInput').value, $('#depInput').value, $('#modelInput').value
    ));
    $('#submit').addEventListener('click', ev => run());
    $('#input').addEventListener('keydown', ev => (event.keyCode == 13) && run());
    $('#download').addEventListener('click', ev => $('#download').setAttribute('href', downloadSVG()).click());


    // Update View

    const updateView = (text, model, settings, server, depApi, modelApi) => {
        $('#input').value = text;
        if (modelData?.length) {
            updateModelView($('#models > ul'), $('#modelDummy-dummy').parentElement);
            model = model || modelData[0];
            $(`[value="${model}"]`).checked = true;
        }
        $('#settings-punctuation').checked = settings.collapsePunct;
        $('#settings-phrases').checked = settings.collapsePhrase;
        $('#serverInput').value = server;
        $('#depInput').value = depApi;
        $('#modelInput').value = modelApi;
    };

    const updateModelView = (container, dummy) => {
        while (container.firstElementChild) {
            container.removeChild(container.firstElementChild);
        }
        for (const model of modelData) {
            const copy = dummy.cloneNode(true);
            const input = copy.children[0];
            input.name = 'model';
            input.value = model;
            input.id = 'model-' + model;
            const label = copy.children[1];
            label.htmlFor = input.id;
            label.firstChild.remove();
            label.prepend(model);
            container.appendChild(copy);
        }
    };


    // Update URL

    const updateURL = (text, model, settings, server, depApi, modelApi) => {
        const url = [
            'server=' + encodeURIComponent(server),
            'depApi=' + encodeURIComponent(depApi),
            'modelApi=' + encodeURIComponent(modelApi),
            'text=' + encodeURIComponent(text),
            'model=' + encodeURIComponent(model),
            'cpu=' + (settings.collapsePunct ? 1 : 0),
            'cph=' + (settings.collapsePhrase ? 1 : 0)
        ];

        history.pushState({ text, model, settings, server, depApi, modelApi }, null, '?' + url.join('&'));
    }

    // Get URL Query Variables

    const getQueryVar = (key) => {
        const query = window.location.search.substring(1);
        const params = query.split('&').map(param => param.split('='));

        for (let param of params)
            if (param[0] == key) return decodeURIComponent(param[1]);
        return false;
    }


    // Download SVG

    const downloadSVG = () => {
        const serializer = new XMLSerializer();
        return ($('#displacy-svg')) ? 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent('<?xml version="1.0" standalone="no"?>\r\n' + serializer.serializeToString($('#displacy-svg'))) : false;
    }
}